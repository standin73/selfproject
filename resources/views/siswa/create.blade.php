@extends('layouts.master')

@section('title')
    <h3>Tambah Data</h3>
@endsection

@section('content')
<div class="card-body">
    <form class="forms-sample" method="POST" action="/siswa" enctype="multipart/form-data">
        @csrf
      <div class="form-group">
        <label for="Nama">Nama</label>
        <input type="text" class="form-control" id="Nama" name="Nama" placeholder="Nama Lengkap">
      </div>
      <div class="form-group">
        <label for="NISN">NISN</label>
        <input type="text" class="form-control" name="NISN" id="NISN" placeholder="NISN">
      </div>
      <div class="form-group">
        <label for="Jenis_kelamin">Gender</label>
        <select class="form-control" id="Jenis_kelamin" name="Jenis_kelamin">
          <option value="L">Pria</option>
          <option value="P">Wanita</option>
        </select>
      </div>
      <div class="form-group">
        <label for="Gambar">Upload Foto</label>
        <input type="file" class="form-control-file" name="gambar" id="Gambar">
      </div>
      <div class="form-group">
        <label for="Kelas">Kelas</label>
        <select class="form-control" id="Kelas" name="kelas_id">
          <option>---- Pilih kelas ----</option>
          @foreach ($kelas as $item)
              <option value="{{$item->id}}">{{$item->Kelas}}</option>
          @endforeach
        </select>
      </div>
      <button type="submit" class="btn btn-primary mr-2">Submit</button>
      <button class="btn btn-dark">Cancel</button>
    </form>
  </div>

@endsection