@extends('layouts.master')

@section('title')
    <h3>Daftar Siswa</h3>
    <a class="btn btn-primary mb-2" href="/siswa/create" role="button">Tambah Data</a>
@endsection


@section('content')
<table class="table">
    <thead>
    <tr class="table-striped">
      <th>No</th>
      <th>Gambar</th>
      <th>Nama</th>
      <th>NISN</th>
      <th>Jenis Kelamin</th>
      <th>Kelas</th>
      <th>Aksi</th>
    </tr>
    </thead>
    <tbody> 
      @forelse ($siswa as $key=>$value)
      <tr>
        <td>{{$key + 1}}</td>
        <td><img src="{{asset('gambar/' . $value->gambar)}}" class="card-img-top" alt="..."></td>
        <td>{{$value->Nama}}</td>
        <td>{{$value->NISN}}</td>
        <td>{{$value->Jenis_kelamin}}</td>
        <td>{{$value->kelas->Kelas}}</td>
        <td>
          <form action="/siswa/{{$value->id}}" method="POST">
            @csrf
            @method("DELETE")
            <a href="/siswa/{{$value->id}}" class="btn btn-primary btn-sm">Detail</a>
            <a href="/siswa/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
          </form>
        </td>
      </tr>
      @empty
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Tidak Ada data!</strong> 
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endforelse
    </tbody>
</table>
@endsection
