@extends('layouts.master')

@section('title')
    <h3>Detail Data</h3>
@endsection

@section('content')
<div class="card-body">
      <div class="form-group">
        <label for="NISN">NISN</label>
        <input type="text" class="form-control" name="NISN" id="NISN" value="{{$siswa->NISN}}">
      </div>
      <div class="form-group">
        <label for="Nama">Name</label>
        <input type="text" class="form-control" id="Nama" name="Nama" value="{{$siswa->Nama}}">
      </div>
      <div class="form-group">
        <label for="Jenis_kelamin">Gender</label>
        <input type="text" class="form-control" name="Jenis_kelamin" value="{{$siswa->Jenis_kelamin}}">
      </div>
      <div class="form-group">
        <label for="Gambar">Upload Foto</label>
        <input type="file" class="form-control-file" name="gambar" id="Gambar">
      </div>
      <div class="form-group">
        <label for="Kelas">Kelas</label>
        <select class="form-control" id="Kelas" name="kelas_id">
          @foreach ($kelas as $item)
              @if ( $item->id == $siswa->kelas_id)
              <option value="{{$item->id}}" selected>{{$item->Kelas}}</option> 
              @else
              <option value="{{$item->id}}">{{$item->Kelas}}</option>
              @endif  
          @endforeach
        </select>
      </div>
  </div>

@endsection