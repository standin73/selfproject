@extends('layouts.master')

@section('title')
    <h3>Edit Data</h3>
@endsection

@section('content')
<div class="card-body">
    <form action="/siswa/{{$siswa->id}}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label for="NISN">NISN</label>
        <input type="text" class="form-control" name="NISN" id="NISN" value="{{$siswa->NISN}}">
      </div>
      @error('NISN')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label for="Nama">Name</label>
        <input type="text" class="form-control" id="Nama" name="Nama" value="{{$siswa->Nama}}">
      </div>
      @error('Name')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label for="Jenis_kelamin">Gender</label>
        <input type="text" class="form-control" name="Jenis_kelamin" value="{{$siswa->Jenis_kelamin}}">
      </div>
      @error('Jenis_kelamin')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label for="Gambar">Update Foto</label>
        <img src="{{ asset('gambar/'. $siswa->gambar)}}" alt="" height="140" width="140">
        <input type="file" class="form-control-file" name="gambar" id="Gambar">
      </div>
      @error('gambar')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label for="Kelas">Kelas</label>
        <select class="form-control" id="Kelas" name="kelas_id">
          @foreach ($kelas as $item)
              @if ( $item->id == $siswa->kelas_id)
              <option value="{{$item->id}}" selected>{{$item->Kelas}}</option> 
              @else
              <option value="{{$item->id}}">{{$item->Kelas}}</option>
              @endif  
          @endforeach
        </select>
      </div>
      @error('kelas')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary mr-2">Edit</button>
      <button class="btn btn-dark">Cancel</button>
    </form>
  </div>

@endsection