<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
    <a class="sidebar-brand brand-logo" href="index.html"><img src="{{asset("Admin/template/assets/images/logo.svg")}}" alt="logo" /></a>
    <a class="sidebar-brand brand-logo-mini" href="index.html"><img src="{{asset("Admin/template/assets/images/logo-mini.svg")}}" alt="logo" /></a>
  </div>
  <ul class="nav">
    <li class="nav-item profile">
      <div class="profile-desc">
        <div class="profile-pic">
          <div class="count-indicator">
            <img class="img-xs rounded-circle " src="{{asset("Admin/template/assets/images/faces/face15.jpg")}}" alt="">
            <span class="count bg-success"></span>
          </div>
          <div class="profile-name">
            <h5 class="mb-0 font-weight-normal">Hasan Ali</h5>
            <span>Gold Member</span>
          </div>
        </div>
      </div>
    </li>
    <li class="nav-item nav-category">
      <span class="nav-link">Navigation</span>
    </li>
    <li class="nav-item menu-items">
      <a class="nav-link" href="/">
        <span class="menu-icon">
          <i class="mdi mdi-speedometer"></i>
        </span>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>
    <li class="nav-item menu-items">
      <a class="nav-link"href="/siswa">
        <span class="menu-icon">
          <i class=" mdi mdi-account"></i>
        </span>
        <span class="menu-title">Siswa</span>
      </a>
    </li>
    <li class="nav-item menu-items">
      <a class="nav-link" href="/nilai">
        <span class="menu-icon">
          <i class="mdi mdi-email-outline"></i>
        </span>
        <span class="menu-title">Upload Nilai</span>
      </a>
    </li>
    <li class="nav-item menu-items">
      <a class="nav-link" href="/walisiswa">
        <span class="menu-icon">
          <i class="mdi mdi-laptop"></i>
        </span>
        <span class="menu-title">Wali Siswa</span>
      </a>
    </li>
    <li class="nav-item menu-items">
      <a class="nav-link" href="/kelas">
        <span class="menu-icon">
          <i class="mdi mdi-email-outline"></i>
        </span>
        <span class="menu-title">Kelas</span>
      </a>
    </li>
    <li class="nav-item menu-items">
      <a class="nav-link" href="/mapel">
        <span class="menu-icon">
          <i class="mdi mdi-email-outline"></i>
        </span>
        <span class="menu-title">Mata pelajaran</span>
      </a>
    </li>
  </ul>
</nav>