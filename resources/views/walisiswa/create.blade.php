@extends('layouts.master')

@section('title')
    <h3>Tambah Walisiswa</h3>
@endsection

@section('content')
<div class="card-body">
    <form class="forms-sample" method="POST" action="/walisiswa">
        @csrf
      <div class="form-group">
        <label for="Nama">Nama</label>
        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap">
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="Telepon">Telepon</label>
        <input type="text" class="form-control" name="telepon" id="Telepon" placeholder="Nomor Telepon">
      </div>
      @error('telepon')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label for="alamat">Alamat</label>
        <textarea  class="form-control" name="alamat" id="alamat" cols="30" rows="10" placeholder="Alamat"></textarea>
        @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <button type="submit" class="btn btn-primary mr-2">Submit</button>
      <button class="btn btn-dark">Cancel</button>
    </form>
  </div>

@endsection