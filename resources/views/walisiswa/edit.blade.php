@extends('layouts.master')

@section('title')
    <h3>Edit Data Walisiswa</h3>
@endsection

@section('content')
<div class="card-body">
    <form action="/walisiswa/{{$walisiswa->id}}" method="POST">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" value="{{$walisiswa->nama}}">
      </div>
      @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label for="telepon">Telepon</label>
        <input type="text" class="form-control" id="telepon" name="telepon" value="{{$walisiswa->telepon}}">
      </div>
      @error('telepon')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label for="alamat">Alamat</label>
        <textarea  class="form-control" name="alamat" id="alamat" cols="30" rows="10" placeholder="Alamat">{{$walisiswa->alamat}}</textarea>
        @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <button type="submit" class="btn btn-primary mr-2">Edit</button>
      <button class="btn btn-dark">Cancel</button>
    </form>
  </div>

@endsection