@extends('layouts.master')

@section('title')
    <h3>Upload Nilai Siswa</h3>
    <a class="btn btn-primary mb-2" href="/nilai/create" role="button">Upload Nilai</a>
@endsection


@section('content')
<table class="table">
    <thead>
    <tr class="table-striped">
      <th>No</th>
      <th>Nilai</th>
      <th>Aksi</th>
    </tr>
    </thead>
    <tbody>
      @forelse ($nilai as $key=>$value)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$value->nilai}}</td>
          <td>
            <form action="/nilai/{{$value->id}}" method="POST">
              @csrf
              @method("DELETE")
              <a href="/nilai/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
          </td>
        </tr>
      @empty
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Tidak Ada data!</strong> 
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endforelse
    </tbody>
  </table>
@endsection
