@extends('layouts.master')

@section('title')
    <h3>Upload Nilai Siswa</h3>
@endsection

@section('content')
<div class="card-body">
    <form class="forms-sample" method="POST" action="/nilai" >
        @csrf
      <div class="form-group">
        <label for="Nilai">Nilai</label>
        <input type="text" class="form-control" id="Nilai" name="nilai" placeholder="Masukkan Nilai">
      </div>
      <button type="submit" class="btn btn-primary mr-2">Submit</button>
      <button class="btn btn-dark">Cancel</button>
    </form>
  </div>

@endsection