@extends('layouts.master')

@section('title')
    <h3>Edit Nilai</h3>
@endsection

@section('content')
<div class="card-body">
    <form class="forms-sample" action="/nilai/{{$nilai->id}}" method="POST">
        @csrf
        @method('PUT')
      <div class="form-group">
        <label for="Nilai">Nilai</label>
        <input type="text" class="form-control" id="Nilai" name="nilai" value="{{$nilai->nilai}}">
      </div>
      <button type="submit" class="btn btn-primary mr-2">Submit</button>
      <button class="btn btn-dark">Cancel</button>
    </form>
  </div>

@endsection