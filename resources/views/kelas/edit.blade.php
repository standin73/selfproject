@extends('layouts.master')

@section('title')
    <h3>Edit Data</h3>
@endsection

@section('content')
<div class="card-body">
    <form action="/kelas/{{$kelas->id}}" method="POST">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label for="Kelas">Kelas</label>
        <input type="text" class="form-control" name="kelas" id="kelas" value="{{$kelas->Kelas}}">
      </div>
      @error('kelas')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary mr-2">Edit</button>
      <button class="btn btn-dark">Cancel</button>
    </form>
  </div>

@endsection