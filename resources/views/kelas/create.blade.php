@extends('layouts.master')

@section('title')
    <h3>Tambah Kelas</h3>
@endsection

@section('content')
<div class="card-body">
    <form class="forms-sample" method="POST" action="/kelas">
        @csrf
      <div class="form-group">
        <label for="Kelas">Kelas</label>
        <input type="text" class="form-control" id="Kelas" name="kelas">
      </div>
      <button type="submit" class="btn btn-primary mr-2">Submit</button>
      <button class="btn btn-dark">Cancel</button>
    </form>
  </div>

@endsection