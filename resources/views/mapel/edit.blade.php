@extends('layouts.master')

@section('title')
    <h3>Edit Mata Pelajaran</h3>
@endsection

@section('content')
<div class="card-body">
    <form action="/mapel/{{$mapel->id}}" method="POST">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label for="mapel">mapel</label>
        <input type="text" class="form-control" name="nama_mapel" id="mapel" value="{{$mapel->nama_mapel}}">
      </div>
      @error('mapel')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary mr-2">Edit</button>
      <button class="btn btn-dark">Cancel</button>
    </form>
  </div>

@endsection