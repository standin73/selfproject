@extends('layouts.master')

@section('title')
    <h3>Tambah Mata Pelajaran</h3>
@endsection

@section('content')
<div class="card-body">
    <form class="forms-sample" method="POST" action="/mapel">
        @csrf
      <div class="form-group">
        <label for="Kelas">Kelas</label>
        <input type="text" class="form-control" id="Kelas" name="nama_mapel">
      </div>
      <button type="submit" class="btn btn-primary mr-2">Submit</button>
    </form>
  </div>

@endsection