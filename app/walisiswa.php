<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class walisiswa extends Model
{
    protected $table = 'wali_siswa';
    protected $fillable = ['nama', 'alamat', 'telepon'];
}
