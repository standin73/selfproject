<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = "Kelas";
    protected $fillable = ['Kelas']; 

    public function siswa()
    {
        return $this->hasOne('App\siswa');
    }

    public function nilai()
    {
        return $this->belongsTo('App\nilai');
    }

    // public function kelas()
    // {
    //     return $this->belongsTo('App\siswa');
    // }
}


