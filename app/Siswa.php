<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class siswa extends Model
{
    protected $table = "siswa";
    protected $fillable = ['NISN', 'Nama' , 'Jenis_kelamin', 'gambar', 'kelas_id','nilai_id'];

    public function kelas()
    {
        return $this->belongsTo('App\kelas');
    }

    public function nilai()
    {
        return $this->hasOne('App\nilai');
    }
}
