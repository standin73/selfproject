<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    protected $table = 'Mapel';
    protected $fillable = ['Nama_mapel'];

    // public function nilai()
    // {
    //     return $this->belongsTo('App\nilai');
    // }
}
