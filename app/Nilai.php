<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    protected $table = 'nilai';
    protected $fillable = ['nilai'];

    public function siswa()
    {
        return $this->belongsTo('App\siswa');
    }
}
