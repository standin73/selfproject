<?php

namespace App\Http\Controllers;
use App\siswa;
use App\kelas;
use Illuminate\Http\Request;
use File;
use DB;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswa = siswa::all();
        return view('siswa.index', compact('siswa'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelas = kelas::all();
        return view('siswa.create', compact('kelas')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Nama' => 'required',
            'NISN' =>  'required',
            'Jenis_kelamin' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'kelas_id' => 'required'
        ]);

        $gambarnama = time().'.'.$request->gambar->extension(); 

        $request->gambar->move(public_path('gambar'), $gambarnama);

        $siswa = new Siswa;
        $siswa->Nama = $request->Nama;
        $siswa->NISN = $request->NISN;
        $siswa->Jenis_kelamin = $request->Jenis_kelamin;
        $siswa->gambar = $gambarnama;
        $siswa->kelas_id = $request->kelas_id;

        $siswa->save();

        return redirect()->route('siswa.index')->with('success', 'Data berhasil terupdate!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $siswa = siswa::FindOrFail($id);
        // $kelas = DB::table('kelas')->get();
        // return view('siswa.show', compact('siswa','kelas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = siswa::FindOrFail($id);
        $kelas = DB::table('kelas')->get();
        return view('siswa.edit', compact('siswa','kelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'Nama' => 'required',
            'NISN' =>  'required',
            'Jenis_kelamin' => 'required',
            'gambar' => 'image|mimes:jpeg,png,jpg|max:2048',
            'kelas_id' => 'required'
        ]);

        
        if ($request->has('gambar')) {
            $siswa = Siswa::find($id);
            $path = 'gambar/';
            File::delete($path .  $siswa->gambar);
            $gambarnama = time().'.'.$request->gambar->extension(); 
            $request->gambar->move(public_path('gambar/'), $gambarnama);

            $siswa->Nama = $request->Nama;
            $siswa->NISN = $request->NISN;
            $siswa->Jenis_kelamin = $request->Jenis_kelamin;
            $siswa->gambar = $gambarnama;
            $siswa->kelas_id = $request->kelas_id;

            $siswa->save();

            return redirect('/siswa');
        } else {
            $siswa = Siswa::find($id);
            $siswa->Nama = $request->Nama;
            $siswa->NISN = $request->NISN;
            $siswa->Jenis_kelamin = $request->Jenis_kelamin;
            $siswa->kelas_id = $request->kelas_id;

            $siswa->save();
            return redirect('/siswa');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $siswa = Siswa::find($id);

        $path = "gambar/";
        File::delete($path. $siswa->gambar);
        $siswa->delete();

        return redirect('/siswa');

    }
}
