<?php

namespace App\Http\Controllers;
use App\walisiswa;
use Illuminate\Http\Request;

class WalisiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $walisiswa = walisiswa::all();
        return view('walisiswa.index', compact('walisiswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('walisiswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'telepon' => 'required',
            'alamat' => 'required'
        ]);

        $walisiswa = new Walisiswa;

        $walisiswa->nama = $request->nama;
        $walisiswa->telepon = $request->telepon; // int maks 10 angka
        $walisiswa->alamat = $request->alamat;

        $walisiswa->save();

        return redirect('/walisiswa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $walisiswa = walisiswa::FindOrFail($id);
        return view('walisiswa.edit',compact('walisiswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'telepon' => 'required',
            'alamat' => 'required'
        ]);

        $walisiswa = Walisiswa::find($id);

        $walisiswa->nama = $request->nama;
        $walisiswa->telepon = $request->telepon;
        $walisiswa->alamat = $request->alamat;

        $walisiswa->save();

        return redirect('/walisiswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $walisiswa = walisiswa::find($id);

        $walisiswa->delete();

        return redirect('/walisiswa');
    }
}
